import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireModule } from 'angularfire2';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import { MatTabsModule, MatTableModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatCard, MatNativeDateModule } from '@angular/material';
import { MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule} from '@angular/material/icon';
import { YagaModule } from '@yaga/leaflet-ng2';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SimuladorComponent } from './simulador/simulador.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';

var firebaseConfig = {
  apiKey: "AIzaSyBQTNC0c9rXKcNeut3fzwKyUuhzEQ98Odw",
  authDomain: "banco-de-la-alegria.firebaseapp.com",
  databaseURL: "https://banco-de-la-alegria.firebaseio.com",
  projectId: "banco-de-la-alegria",
  storageBucket: "banco-de-la-alegria.appspot.com",
  messagingSenderId: "292334414915"
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SimuladorComponent,
    FooterComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatExpansionModule,
    MatCardModule,
    MatGridListModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatIconModule,
    FormsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
