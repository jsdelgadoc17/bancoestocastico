import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import { MatButtonModule} from '@angular/material/button';
import { MatInputModule, MatFormFieldModule, MatSelectModule, MatNativeDateModule, MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  sucursales : AngularFirestoreCollection<any>;
  sucursales$ : any;
  sucursalSeleccionada : any;

  franjas : AngularFirestoreCollection<any>;
  franjas$ : any;
  franjaSeleccionada : string;

  tiempoEnBanco : number;
  tiempoEspera : number;
  PersonasEnFila : number;
  probAtencionInmediata : number;

  maxTurnosParaAsignar : number;

  franjaActual : AngularFirestoreCollection<any>;
  franjaActual$ : any;
  turnoIrAhora : AngularFirestoreCollection<any>;
  turnoIrAhora$ : any;
  filaActual: any;
  mensajeIrAhora : string;

  mensajeTurnoComunAhora : string;
  mensajeAsignacion : string;

  myDate: Date;

  constructor( private afs : AngularFirestore ) { }

  ngOnInit() {

    this.sucursales = this.afs.collection('sucursales', ref => ref.orderBy('nombre'))
    this.sucursales$ = this.sucursales.snapshotChanges()
    .map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, data };
      });
    });
      //  this.utcTime()  
    }

    
  queue (llegada, servicio, servidores){
    var p = llegada / (servidores * servicio);
    var r = llegada / servicio;
    var p0 = 0;
    for (var i = 0; i <= servidores; i++){
        p0 += Math.pow(r, i) / this.factorial(i);
    }
    p0 += Math.pow(r, servidores) / (this.factorial(servidores)*(1-p));
    p0 = 1 / p0;
    var w = (1/servicio) + (Math.pow(r, servidores)/(this.factorial(servidores)*(servidores*servicio)*Math.pow((1-p),2)))*p0;
    var lq = ((Math.pow(r, servidores)*p)/(this.factorial(servidores)*Math.pow((1-p),2)))*p0;
    var wq = lq / llegada;
    return [w, wq, lq, p0];
  }

  factorial (n) {
    var total = 1; 
    for (var i = 1 ; i <= n ; i++) {
      total = total * i; 
    }
    return total; 
  }  

  setSucursal( $event ){
    this.sucursalSeleccionada = $event.value;
    this.franjas = this.afs.collection('franjaInfo',  ref => ref.where('sucursal', '==', $event.value.data.nombre ) )
    this.franjas$ = this.franjas.snapshotChanges()
    .map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, data };
      });
    });    

  }

  mostrarInfo($event){  
    //franja seleccionada
    this.franjaSeleccionada = $event.value;

    //calculo segun teoria de colas
    var t : any[] = this.queue( $event.value.data.llegadas, $event.value.data.servicio, $event.value.data.servidores);
    //Asignacion de valores para mensajes <li>, suponiendo que esta en minutos
    this.tiempoEnBanco =  Math.floor (t[0]*60)
    this.tiempoEspera =  Math.floor (t[1]*60*60)//segundos
    this.PersonasEnFila = Math.floor(t[2])
    this.probAtencionInmediata =  Math.floor((t[3])*100 )
    //numero maximo de personas que pueden reservar en dicha franja
    this.maxTurnosParaAsignar = $event.value.data.servicio;

  }

  reservarTurno(){
    let aux = new Date();
    let cod = "R" +  (this.franjaSeleccionada["data"].inicio +  this.franjaSeleccionada["data"].fin + aux.getMilliseconds());
    //console.log(this.franjaSeleccionada["data"].inicio);
    this.afs.collection('reservas').add(
      {
        'codigo': cod, 
        'horaLlegada': this.franjaSeleccionada["data"].inicio,
        'sucursal' : this.sucursalSeleccionada.data.nombre}
      );
    this.mensajeAsignacion = "El código de reserva es: " + cod;
  } 

  irAhora(){
    let dateAux = new Date();

    this.franjaActual = this.afs.collection('franjaInfo', ref => ref.where('inicio', '==', dateAux.getHours()).where('sucursal', '==',this.sucursalSeleccionada.data.nombre ).limit(1));
    this.franjaActual.snapshotChanges()
    .subscribe(v => {
      this.franjaActual$ = v;      
      this.turnoIrAhora = this.afs.collection('sucursales', ref => ref.where('nombre', '==', this.sucursalSeleccionada.data.nombre).limit(1));
      this.turnoIrAhora.valueChanges()
      .subscribe(v => {
        this.turnoIrAhora$ = v;      
        this.filaActual = this.turnoIrAhora$[0].cola.length;
        this.mensajeIrAhora = "En este momento hay: " + this.filaActual 
                              + " turnos, el tuyo se atendería en aproximadamente "
                              + Math.round(this.franjaActual$[0].payload.doc.data().servicio * (1/60) * this.filaActual)
                              + " minutos";
        
      });
    });

  }  

  encolar( ){

    let cod = "C" + (Math.trunc(Math.random() * (999 - 100) + 100));
    let antiguaCola : any [] = this.sucursalSeleccionada.data.cola;
    antiguaCola.push( cod );
    this.turnoIrAhora.doc( this.sucursalSeleccionada.id ).update({
      cola : antiguaCola
      })
    this.mensajeTurnoComunAhora = "Tu turno es: " + cod
  }

  utcTime(): void {
    setInterval(() => {
        this.myDate = new Date();
        console.log(this.myDate); // just testing if it is working
    }, 10000);
  }
}
