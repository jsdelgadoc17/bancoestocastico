import { Component, OnInit, NgModule } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { MatCardModule} from '@angular/material/card';
import { MatInputModule, MatFormFieldModule, MatSelectModule, MatNativeDateModule, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { MatIconModule} from '@angular/material/icon';
import {FormsModule} from '@angular/forms'
@Component({
  selector: 'simulador',
  templateUrl: './simulador.component.html',
  styleUrls: ['./simulador.component.css']
})
export class SimuladorComponent implements OnInit {

  sucursales : AngularFirestoreCollection<any>;
  sucursales$ : any;
  sucursalSeleccionada : any;

  cajeros : AngularFirestoreCollection<any>;
  cajeros$ : any;

  colaSeleccionada : AngularFirestoreDocument<any>;
  colaSeleccionada$ : any;
  
  cola : any;

  numReserva : number;
  reserva : AngularFirestoreCollection<any>;
  reserva$ : any;

  constructor( private afs : AngularFirestore ) { }

  ngOnInit() {

    this.sucursales = this.afs.collection('sucursales', ref => ref.orderBy('nombre'))
    this.sucursales$ = this.sucursales.snapshotChanges()
    .map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, data };
      });
    });

    this.cajeros = this.afs.collection('cajeros', ref => ref.orderBy('nombre'))
    this.cajeros$ = this.cajeros.snapshotChanges()
   
    .map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, data };
      });
    });
  }

  setSucursal( $event ){

    this.sucursalSeleccionada = $event.value;

    this.colaSeleccionada = this.afs.doc('sucursales/' + this.sucursalSeleccionada.id);
    this.colaSeleccionada.valueChanges()
      .subscribe( v =>{
        this.colaSeleccionada$ = v.cola
      });

  }
  valueChange(value){
      this.numReserva = value;
    }

  encolar( esReserva ){

    let antiguaCola : any [] = this.sucursalSeleccionada.data.cola;
    
    if ( esReserva ){

      for( var i = antiguaCola.length - 1; i >= 0; i-- ){

        if( antiguaCola[i].includes("R") ){
          antiguaCola.splice(i + 1, 0, this.reserva$[0].payload.doc.data().codigo )
          break;
        } else if( i == 0){ 
          antiguaCola.splice( 0 , 0,  this.reserva$[0].payload.doc.data().codigo)
          break;
        }
      }
      this.sucursales.doc( this.sucursalSeleccionada.id ).update({
        cola : antiguaCola
        })

    } else {

      let cod = "C" + ( Math.trunc(Math.random() * (999 - 100) + 100));
      antiguaCola.push( cod );
      this.sucursales.doc( this.sucursalSeleccionada.id ).update({
        cola : antiguaCola
        })
    }
  }

  encolarReserva( ){
  
    this.reserva = this.afs.collection('reservas', ref => ref.where('codigo', '==', this.numReserva).limit(1));
    this.reserva.snapshotChanges()
      .subscribe(v =>{
        this.reserva$ = v;
        this.encolar( true );
      })
  }

  encolarComun(){

    this.encolar( false );

  }

  clienteAtendido(){

    let antCola : any [] = this.sucursalSeleccionada.data.cola;
    antCola.splice(0,1);

    this.sucursales.doc( this.sucursalSeleccionada.id ).update({
      cola : antCola
      })
  }


}
